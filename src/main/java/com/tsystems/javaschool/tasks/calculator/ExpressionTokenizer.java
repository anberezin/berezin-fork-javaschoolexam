package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;

/**
 * Provides a partition of arithmetic expression string into tokens.
 * Tokens are elements of arithmetic expression: numbers, brackets, operators.
 */
public class ExpressionTokenizer {
    /**
     * Tokenization of arithmetic expression string into tokens.
     * @param expression initial arithmetic expression
     * @return array with tokens
     * @throws Exception at errors in tokenization, additional details in message
     */
    public ArrayList<String> getExpressionTokens (String expression) throws Exception {
        if (expression.length() == 0)
            throw new Exception("Initial string is empty");

        ArrayList<String> result;
        StringBuilder currentToken;
        char currentSymbol;
        ArithmeticElements.SymbolType typeOfCurrentSymbol;
        ArithmeticElements.SymbolType typeOfPreviousSymbol;

        result = new ArrayList<>();
        currentToken = new StringBuilder();

        typeOfPreviousSymbol = ArithmeticElements.SymbolType.UNKNOWN;

        for (int i = 0; i < expression.length(); i++) {
            currentSymbol = expression.charAt(i);
            typeOfCurrentSymbol = ArithmeticElements.getTypeOfSymbol(currentSymbol);

            if (typeOfCurrentSymbol == ArithmeticElements.SymbolType.UNKNOWN) {
                if (currentSymbol == ' ')
                    continue;
                throw new Exception("Encountered incorrect symbol in initial string.");
            }

            if ((typeOfCurrentSymbol == ArithmeticElements.SymbolType.NUMBER && typeOfPreviousSymbol == ArithmeticElements.SymbolType.OPERATOR)
                    || (typeOfPreviousSymbol == ArithmeticElements.SymbolType.NUMBER && typeOfCurrentSymbol == ArithmeticElements.SymbolType.OPERATOR)
                    || (typeOfPreviousSymbol == ArithmeticElements.SymbolType.OPERATOR && typeOfCurrentSymbol == ArithmeticElements.SymbolType.OPERATOR)) {
                result.add(currentToken.toString());
                currentToken.delete(0,currentToken.length());
            }

            currentToken.append(currentSymbol);
            typeOfPreviousSymbol = typeOfCurrentSymbol;
        }

        result.add(currentToken.toString());

        return result;
    }
}