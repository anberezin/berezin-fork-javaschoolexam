package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Provides convertation of arithmetic expression in infix format into Reverse Polish Notation
 */
public class InfixToRpnConverter {
    /**
     * Convertation arithmetic expression in infix format into Reverse Polish Notation (RPN)
     * @param expressionTokens initial expression, as tokens (arithmetic elements) in array, example: {"2", "+", "2"}
     * @return RPN expression in array, example: {"2", "2", "+"}
     */
    public ArrayList<String> convertation(ArrayList<String> expressionTokens) throws Exception {
        ArrayList<String> result;
        Stack<String> stackOfOperators;
        String token;
        char operator;
        boolean nextNumberIsNegative;

        result = new ArrayList<>();
        stackOfOperators = new Stack<>();

        for (int tokenIndex = 0; tokenIndex < expressionTokens.size(); tokenIndex++) {
            token = expressionTokens.get(tokenIndex);
            try {
                Double.parseDouble(token);
                result.add(token);
                continue;
            } catch (NumberFormatException ignore) {
            }

            if (token.length() > 1)
                throw new Exception("Invalid token: \"" + token + "\".");

            if (ArithmeticElements.getTypeOfSymbol(token.charAt(0)) == ArithmeticElements.SymbolType.OPERATOR) {
                operator = token.charAt(0);

                if (operator == '-') {
                    nextNumberIsNegative = false;

                    if (tokenIndex > 0) {
                        if (ArithmeticElements.getTypeOfSymbol(expressionTokens.get(tokenIndex - 1).charAt(0))
                                == ArithmeticElements.SymbolType.OPERATOR
                                && expressionTokens.get(tokenIndex-1).charAt(0) != ')' &&
                                expressionTokens.get(tokenIndex-1).charAt(0) != '+' &&
                                expressionTokens.get(tokenIndex-1).charAt(0) != '-')  {
                            nextNumberIsNegative = true;
                        }
                    } else {
                        nextNumberIsNegative = true;
                    }

                    // If next number is negative.
                    if (nextNumberIsNegative) {
                        result.add("0");
                        stackOfOperators.push("-");
                        continue;
                    }
                }

                if (stackOfOperators.empty()) {
                    stackOfOperators.push(String.valueOf(operator));
                    continue;
                }

                if (operator == '(') {
                    stackOfOperators.push(String.valueOf(operator));
                    continue;
                }

                if (operator == ')') {
                    char popedOperator;
                    do {
                        if (stackOfOperators.empty())
                            throw new Exception("Closing bracket not found.");
                        popedOperator = stackOfOperators.pop().charAt(0);
                        if (popedOperator != '(')
                            result.add(String.valueOf(popedOperator));
                    } while (popedOperator != '(');
                    continue;
                }
                while (!stackOfOperators.empty()) {
                    if (ArithmeticElements.getPriorityOfOperator(stackOfOperators.peek().charAt(0))
                            >= ArithmeticElements.getPriorityOfOperator(operator)) {
                        result.add(String.valueOf(stackOfOperators.pop().charAt(0)));
                    } else {
                        break;
                    }
                }
                stackOfOperators.push(String.valueOf(operator));
            }
        }

        while (!stackOfOperators.empty()) {
            result.add(String.valueOf(stackOfOperators.pop()));
        }

        return result;
    }
}
