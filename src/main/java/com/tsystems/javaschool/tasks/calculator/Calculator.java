package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Stack;

public class Calculator {

    public final int COUNT_OF_DIGITS_IN_RESULT = 4;
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null)
            return null;
        ExpressionTokenizer tokenizer;
        ArrayList<String> expressionTokens;
        InfixToRpnConverter converter;
        ArrayList<String> expressionTokensInRpn;
        double result;
        BigDecimal roundedResult;

        try {
            tokenizer = new ExpressionTokenizer();
            expressionTokens = tokenizer.getExpressionTokens(statement);
        } catch (Exception exception) {
            System.out.println("Error on tokenization:    " + exception.getMessage());
            System.out.println("Result:                   null");
            return null;
        }

        System.out.print("Expression tokens:        ");
        for (String row : expressionTokens) {
            System.out.print(row + ", ");
        }

        try {
            converter = new InfixToRpnConverter();
            expressionTokensInRpn = converter.convertation(expressionTokens);
        } catch (Exception exception) {
            System.out.println("Error on convertation to RPM: " + exception.getMessage());
            System.out.println("Result:                   null");
            return null;
        }

        System.out.print("\nExpression tokens in RPN: ");
        for (String row : expressionTokensInRpn) {
            System.out.print(row + ", ");
        }

        try {
            result = calculateExpressionInRpm(expressionTokensInRpn);
            roundedResult = new BigDecimal(result);
            roundedResult = roundedResult.setScale(COUNT_OF_DIGITS_IN_RESULT ,BigDecimal.ROUND_HALF_DOWN);
            DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
            decimalFormatSymbols.setDecimalSeparator('.');
            DecimalFormat df = new DecimalFormat("###.####", decimalFormatSymbols);
            return df.format(roundedResult);
        } catch (Exception exception) {
            // On some error in calculation. Example, dividing by zero or incorrect order of elements
            System.out.println(exception.getMessage());
            System.out.println("Result:                   null");
            return null;
        }
    }

    public double calculateExpressionInRpm(ArrayList<String> expressionTokensInRpn) throws Exception {
        Stack<Double> stackOfNumbers;
        double resultOfOperation = 0;
        double firstNumber;
        double secondNumber;
        double parsedNumber;
        double result;

        stackOfNumbers = new Stack<>();

        System.out.print("\nOperations:               ");
        for (String token : expressionTokensInRpn) {
            try {
                parsedNumber = Double.parseDouble(token);
                stackOfNumbers.push(parsedNumber);
                continue;
            } catch (NumberFormatException ignore) {
            }

            if (ArithmeticElements.getTypeOfSymbol(token.charAt(0)) == ArithmeticElements.SymbolType.OPERATOR) {
                try {
                    secondNumber = stackOfNumbers.pop();
                    firstNumber = stackOfNumbers.pop();
                } catch (EmptyStackException exception) {
                    throw new Exception("Incorrect order of the elements or extra element in the initial expression.");
                }

                switch (token.charAt(0)) {
                    case '+':
                        resultOfOperation = firstNumber + secondNumber;
                        break;
                    case '-':
                        resultOfOperation = firstNumber - secondNumber;
                        break;
                    case '*':
                        resultOfOperation = firstNumber * secondNumber;
                        break;
                    case '/':
                        try {
                            if (secondNumber == 0.0d)
                                throw new Exception("Dividing by zero.");
                            resultOfOperation = firstNumber / secondNumber;
                        } catch (ArithmeticException dividingException) {
                            throw new Exception("Dividing by zero.");
                        }
                        break;
                }

                System.out.print(firstNumber + token + secondNumber + "=" + resultOfOperation + "; ");
                stackOfNumbers.push(resultOfOperation);
            }
        }

        if (stackOfNumbers.size() > 1) {
            System.out.print("\nError: In stack " + stackOfNumbers.size() + " element(s).");
            throw  new Exception("Incorrect order of elements or extra element in the initial expression .");
        }
        result = stackOfNumbers.pop();
        return result;
    }

}
