package com.tsystems.javaschool.tasks.calculator;

public class ArithmeticElements {
    public enum SymbolType { OPERATOR, NUMBER, DOT, UNKNOWN }

    public static int getPriorityOfOperator(char operator) {
        switch (operator) {
            case ')': return 3;
            case '*': return 2;
            case '/': return 2;
            case '+': return 1;
            case '-': return 1;
            case '(': return 0;
        }
        return -1;
    }

    /**
     * Get symbol type (operator, number, dot or unknown)
     * @param element symbol to recognition
     * @return type of symbol
     */
    public static SymbolType getTypeOfSymbol(char element) {
        if (element == '-'|| element == '+' || element == '*' ||
                element == '/' || element == '(' || element == ')')
            return SymbolType.OPERATOR;

        if (element >= '0' && element <= '9')
            return SymbolType.NUMBER;

        if (element >= '.')
            return SymbolType.DOT;

        return SymbolType.UNKNOWN;
    }
}