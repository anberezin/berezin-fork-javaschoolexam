package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        if (sourceFile == null || targetFile == null) {
            throw new IllegalArgumentException();
        }
        try {
            outputRowsToFile(readFile(sourceFile), targetFile);
            return true;
        } catch (IOException e) {
            System.out.println("I/O error: " + e.getMessage());
            return false;
        }
    }

    private Map<String, Long> readFile(File inFile) throws IOException {
        return Files.lines(Paths.get(inFile.getAbsolutePath()))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    private void outputRowsToFile(Map<String, Long> rows, File outFile) throws IOException {
        PrintWriter writer = new PrintWriter(new FileWriter(outFile, true));
        rows.entrySet().stream().map(this::toString).forEach(writer::println);
        writer.flush();
        writer.close();
    }

    private String toString(Map.Entry<String, Long> entry) {
        return entry.getKey() + "[" + entry.getValue() + "]";
    }


}
