package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if(x == null || y == null)
            throw new IllegalArgumentException();
        if(x.isEmpty() && y.isEmpty())
            return true;
        else if(x.isEmpty() && !y.isEmpty())
            return true;
        Iterator<Object> xIterator;
        Iterator<Object> yIterator;

        Object currentXObject;
        Object currentYObject;

        xIterator = x.iterator();
        yIterator = y.iterator();

        // Get first object from X
        currentXObject = xIterator.next();

        while (yIterator.hasNext()) {
            // Get next object from Y
            currentYObject = yIterator.next();

            if (currentYObject.equals(currentXObject)) {
                if (xIterator.hasNext()) {
                    // Get next object from X
                    currentXObject = xIterator.next();
                } else {
                    // If list X reached end
                    return true;
                }
            }
        }

        return false;
    }
}
